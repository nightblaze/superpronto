# ASSIGNMENT #

The assignment consist in recreating the menu browsing experience of the iOS app.
Here’s a [video](https://lookback.io/watch/pe4MoAPJ7CWeYzGwB)

### Objectives ###

* Feel free to take some creative freedom and structure informations in other ways, but full screen images and a way to quickly switch between the categories of food and dishes should be maintained
* The way how you structure your project and code matters, please take the time to organise it neatly
* Please do write some tests and include one animation
* Include a short writeup with your assignment, highlighting interesting parts of your work